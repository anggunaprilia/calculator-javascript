const displayHistory = document.querySelector(".display-history");
const display = document.querySelector(".display-input");
const tempResult = document.querySelector(".temp-result");
const numbers = document.querySelectorAll(".number");
const operations = document.querySelectorAll(".operation");
const equal = document.querySelector(".equal");
const clearAll = document.querySelector(".all-clear");
const clearLast = document.querySelector(".last-entity-clear");

let dis1num = "";
let dis2num = "";
let result = null;
let lastOperation= "";
let haveDot = false;

numbers.forEach((number) => {
    number.addEventListener("click", (e) => {
        if(e.target.innerText === "." && !haveDot){
            console.log(e.target.innerText);
            haveDot = true;
        }else if(e.target.innerText === "." && haveDot){
            return;
        }
        dis2num += e.target.innerText;
        display.innerText = dis2num;
    })
});

operations.forEach((operation) => {
    operation.addEventListener("click", (e) => {
        if(!dis2num) return;
        haveDot = false;
        const operationname = e.target.innerText;
        if (dis1num && dis2num && lastOperation){
            mathOperation()
        }else{
            result = parseFloat(dis2num);
        }
        clearVar(operationname)
        lastOperation = operationname
    })
})

function clearVar(name = ""){
    dis1num += dis2num + " " + name + " ";
    displayHistory.innerText =dis1num
    display.innerText = "";
    dis2num = "";
    tempResult.innerText = result;
}

function mathOperation () {
    if(lastOperation === "X"){
        result = parseFloat(result) * parseFloat(dis2num)
    }else if (lastOperation === "+"){
        result = parseFloat(result) + parseFloat(dis2num)
    }
    else if (lastOperation === "-"){
        result = parseFloat(result) - parseFloat(dis2num)
    }
    else if (lastOperation === "/"){
        result = parseFloat(result) / parseFloat(dis2num)
    }
    else if (lastOperation === "%"){
        result = parseFloat(result) % parseFloat(dis2num)
    }
}

equal.addEventListener("click", () => {
    if(!dis1num || !dis2num) return;
    haveDot = false;
    mathOperation();
    clearVar();
    display.innerText = result;
    tempResult.innerText = "";
    dis2num = result;
    dis1num = "";
});

clearAll.addEventListener("click", () => {
    dis1num = "";
    dis2num = "";
    haveDot = false;
    displayHistory.innerText = "";
    display.innerText = "";
    tempResult.innerText = "";
    result = "";
    lastOperation = "";
})

clearLast.addEventListener("click", () => {
    display.innerText = "";
    dis2num = "";
})

window.addEventListener("keydown", (e) => {
    if(
        e.key === "0" ||
        e.key === "1" ||
        e.key === "2" ||
        e.key === "3" ||
        e.key === "4" ||
        e.key === "5" ||
        e.key === "6" ||
        e.key === "7" ||
        e.key === "8" ||
        e.key === "9"
    ){
        clickButton(e.key);
    }else if(e.key === "+" || e.key === "-" || e.key === "/" || e.key === "%"){
        clickOperation(e.key)
    }else if(e.key === "*"){
        clickOperation("X")
    } else if(e.key === "Enter" || e.key === "="){
        clickEqual();
    }else if (e.key === "Backspace"){
        clickClear();
    }
})

function clickButton(key){
    numbers.forEach((button) => {
        if(button.innerText === key){
            button.click()
        }
    })
}

function clickOperation(key){
    operations.forEach((operation) => {
        if(operation.innerText == key){
            operation.click()
        }
    })
}

function clickEqual() {
    equal.click()
}

function clickClear() {
    clearAll.click()
}